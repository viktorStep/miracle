jQuery(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('form[data-toggle="validator"] select').on('change', function (event) {
        event.preventDefault();
        $(this).find('option[disabled]').remove();
    });

    //Custom validator
    $(".custom-validation-form").validator({
        custom: {
            unique: function ($el) {
                var now_date = new Date();
                var now_y = now_date.getFullYear();
                var min_y = now_y - 21;

                var custom_date = new Date($el.val());
                var custom_y = custom_date.getFullYear();

                if (min_y - custom_y < 0 || custom_y < 1930) {
                    return "Hey, your age must be more then 21"
                }
            }
        }
    })

    $('#dob').datetimepicker({
        pickTime: false,
        format: 'MM/DD/YYYY',
        startDate: moment().year(1930),
    });
    
    $('#valid_unti').datetimepicker({
        pickTime: false,
        format: 'MM/DD/YYYY',
        startDate: new Date(),
    });

    $('.add-license').click(function () {
        var registration_state = $('#states').val();
        var medical_license = $('#medical_license').val();
        var valid_until = $('#valid_unti').val();
        var user_id = $('input[name=user_id]').val();
        if (medical_license == ''){
            $('.license-error').html('The medical license is required');
            return;
        }else{
            $('.license-error').html('');
        }
        if (valid_until == ''){
            $('.until-error').html('The valid date is required');
            return;
        }else{
            $('.until-error').html('');
        }
            $.ajax({
                url: '../../doctor/license',
                type: 'post',
                dataType: 'json',
                data: {user_id: user_id, registration_state: registration_state, medical_license: medical_license, valid_until: valid_until},
                success: function (response) {
                    if(response.status == 'success'){
                        window.location.reload();
                    }
                }
            });
    })

})