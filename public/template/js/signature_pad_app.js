
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var wrapper = document.getElementById("signature-pad"),
        clearButton = document.querySelector("[data-action=clear]"),
        saveButton = document.querySelector("[data-action=save]"),
        canvas = document.querySelector("canvas"),
        signaturePad;

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

signaturePad = new SignaturePad(canvas);

clearButton.addEventListener("click", function (event) {
    signaturePad.clear();
});

saveButton.addEventListener("click", function (event) {
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
    } else {
        PrepareImage(signaturePad.toDataURL());
    }
});

function PrepareImage(str) {

    str = str.replace(/^data:image\/(png|jpg);base64,/, "");
    //str = JSON.stringify(str);

    SaveSig(str);

}

function SaveSig(image_string) {

    if (image_string != undefined) {
        $.ajax({
            url: '../../../signature',
            type: 'post',
            dataType: 'json',
            data: {sig_image: image_string},
            success: function (response) {
                if (response.status == 'success') {
                    window.location.reload();
                }
            }
        });

    }

    $("#clear_sig_pad").trigger("click");

}