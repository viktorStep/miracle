var baseUrl = location.protocol + '//' + location.host;

jQuery(document).ready(function () {

    selectSidebarMenu();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.datatable').dataTable();
    $('.post_datepicker, .datepicker').datepicker();


//    $('.editor').wysihtml5();

    $('.recordDelete').click(function () {
        var id = $(this).attr('rel');
        var type = $(this).data('type');

        if (id != '' && confirm('Are you sure you want to delete the ' + type + '?')) {
            window.location.href = '/admin/' + type + '/delete/' + id;
        }
    });

    if ($('#post_id').val() != '') {
        $('.insert-image').removeClass('disabled');
    }

    $('#title').on('blur', function () {
        if ($(this).val() == '')
            $(this).val('Untitled post').change();

        var post_id = $('#post_id').val();
        var data = {};

        if (post_id) {
            data['post_id'] = post_id;
        }
        data['title'] = $(this).val();

        $.ajax({
            url: '/admin/post/create',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (response) {
                var action = $('form').attr('action');

                $('form').attr('action', baseUrl + '/admin/post/' + response.id);
                $('#post_id').val(response.id);
                $('#post-slug').text(baseUrl + '/blog/' + response.id + '/' + response.slug);
                $('#slug-group').removeClass('hidden');
                $('#submit-btn').removeAttr('disabled');

                $('.insert-image').removeClass('disabled');
            }
        });
    });


    $('#statuses').change(function () {
        if ($(this).val() == 'rejected') {
            $('#status_reason').removeClass('hidden');
        } else {
            $('#status_reason').addClass('hidden');
        }
    });


    $('#changeStatus').on('click', function () {
        var status = $('#statuses').val();

        if (status != 0) {
            var data = {};
            data['id'] = $(this).data('id');
            data['status'] = $('#statuses').val();

            if (status == 'rejected') {
                data['status_reason'] = $('#status_reason').val();
            }

            $.ajax({
                url: '/admin/user/status',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {
                    window.location.reload();
                }
            });
        }
    });

    $(document).on('click', '.is_featured', function () {
        if ($(this).is(':checked')) {

            var attachment_id = $(this).data('id');

            var _this = $(this);
            
            $.ajax({
                url: '/admin/post/featured',
                type: 'post',
                dataType: 'json',
                data: {
                    post_id: $('#post_id').val(),
                    attachment_id: attachment_id
                },
                success: function (response) {
                    $('#featuredImage').attr('src', _this.closest('.attachment-item').find('img').attr('src'));
                    $('.modal').modal('hide');
                }
            });
        }
    });

    $('.wysihtml5-sandbox').contents().find('body').on('keyup', function () {
        $('.validate-content').change()
    });
    var postVlaidation = Object.create(blogPostValidation);
    postVlaidation.init();

})



