<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverLicenseToUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('ALTER TABLE `users` ADD `driver_license` VARCHAR(255) NULL DEFAULT NULL AFTER `confirm_token`, ADD `driver_license_image` VARCHAR(255) NULL DEFAULT NULL AFTER `driver_license`, ADD `id_card` VARCHAR(255) NULL DEFAULT NULL AFTER `driver_license_image`, ADD `id_card_image` VARCHAR(255) NULL DEFAULT NULL AFTER `id_card`, ADD `recommendation_image` VARCHAR(255) NULL DEFAULT NULL AFTER `id_card_image`;');
        DB::statement('ALTER TABLE `users` ADD `face_photo` VARCHAR(255) NULL AFTER `recommendation_image`;');
        DB::statement('ALTER TABLE `users` ADD `signature_image` VARCHAR(255) NULL AFTER `face_photo`;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
