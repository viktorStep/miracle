@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')

@section('content')

    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="blog-list">
        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="{{ URL::to('/profile') }}">Profile info</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0)">Edit profile</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/addresses') }}">Addresses</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="edit-profile">
                    <h3>Profile Edit</h3>
                    <div class="x">
                        <div class="">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form method="POST" action="{{ action('Front\AccountController@postEditProfile') }}" data-toggle="validator" role="form" class="custom-validation-form">
                                {!! csrf_field() !!}
                                <div class="form-group has-feedback">
                                    <input type="email" value="{{ auth()->user()->email }}" class="form-control disabled" disabled="disabled">
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="text" name="firstname" value="{{ auth()->user()->first_name }}" class="form-control" id="firstname" placeholder="First Name" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="lastname" value="{{ auth()->user()->last_name }}" class="form-control" id="lastname" placeholder="Last Name" required>
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="date" name="dob" value="{{ auth()->user()->dob }}" class="form-control" id="dob" data-unique="" placeholder="Date of birth" required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <div class="radio radio-inline p-0">
                                        <label>
                                            <input type="radio" name="gender" value="male" id="genderMale" {{ auth()->user()->gender == 'male' ? 'checked' : '' }}>
                                            Male
                                        </label>
                                        <label class="m-l-20">
                                            <input type="radio" name="gender" value="female" id="genderFemale" {{ auth()->user()->gender == 'female' ? 'checked' : '' }}>
                                            Female
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="has_recipe" value="yes" {{ auth()->user()->has_recipe == 'yes' ? 'checked' : '' }}>
                                                Has Recipe
                                            </label>
                                        </div>
                                    </div>

                                    <div class="row m-t-20">
                                        <div class="col-xs-12 text-right">
                                            <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection