@extends('layouts.front.index')
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step in-progress">
                Basic Info
                <div class="node"></div>
            </div>
            <div class="step">
                More Info
                <div class="node"></div>
            </div>
            <div class="step">
                Licensing
                <div class="node"></div>
            </div>
            <div class="step">
                Signing
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{{trans('form.fill_your_data')}}</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postDoctorStep1') }}" data-toggle="validator" role="form" id="registrationForm" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class="form-group has-feedback">
                    <input type="text" name="username"  class="form-control" id="username" placeholder="User name" value="{{session()->has('doctorStep1')? session()->get('doctorStep1')['username']: ''}}"  required >
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="password" name="password" data-minlength="3"  class="form-control" id="inputPassword" placeholder="{{trans('form.password')}}" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation"  class="form-control" id="inputConfirmPassword" placeholder="{{trans('form.confirm_password')}}" data-match="#inputPassword" data-match-error="Whoops, these don't match" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group has-feedback">
                    <input type="email" name="email"  class="form-control" id="email" placeholder="E-mail" value="{{session()->has('doctorStep1')? session()->get('doctorStep1')['email']: ''}}"   required >
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="firstname"  class="form-control" id="firstname" placeholder="{{trans('form.first_name')}}" value="{{session()->has('doctorStep1')? session()->get('doctorStep1')['firstname']: ''}}"  required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="lastname"  class="form-control" id="lastname" placeholder="{{trans('form.last_name')}}" value="{{session()->has('doctorStep1')? session()->get('doctorStep1')['lastname']: ''}}"  required>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group has-feedback date">
                    <input type="text" name="dob"  class="form-control" id="dob" readonly="readonly" value="" placeholder="{{trans('form.date_of_birth')}}" data-unique="" required>
                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                    <div class="help-block with-errors"></div>
                </div>


                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <button type="submit" class="btn btn-primary btn-flat">Next</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
