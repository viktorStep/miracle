@extends('layouts.front.index')
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
                Basic Info
                <div class="node"></div>
            </div>
            <div class="step complete">
                Addresses
                <div class="node"></div>
            </div>
            <div class="step complete">
                Licensing
                <div class="node"></div>
            </div>
            <div class="step complete">
                Profile details
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                Signing
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Signing</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postPatientStep5') }}" data-toggle="validator" role="form" id="registrationForm" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class='form-group'>
                    <div id="signature-pad" class="m-signature-pad">
                        <div class="m-signature-pad--body">
                            <canvas class="signature-class"></canvas>
                        </div>
                        <div class="m-signature-pad--footer">
                            <div class="description">Sign above</div>
                            <button type="button" class="button clear" data-action="clear">Clear</button>
                            <button type="button" class="button save" data-action="save">Save</button>
                        </div>
                    </div>
                </div>
                <div class='form-group sig-image'>
                    @if($user->signature_image)
                    <img src='{{ asset ("/files/profiles/patient-" .$user->id ."/". $user->signature_image)}}' width="160" height="80">
                    @endif
                    <input type="hidden" name="signature_image" value="{{session()->has('patientStep5')? session()->get('patientStep5')['signature_image']: ''}}" required> 
                </div>
                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <div class="col-xs-8">
                            <a href="{{ url('/auth/signup/step-4') }}" class="btn btn-primary btn-flat">Previous</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat">Complete Signup</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
