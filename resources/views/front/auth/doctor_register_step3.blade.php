@extends('layouts.front.index')
@section('content')
<div class="login-content">
    <div class="login-box">
        <div class="wizard-progress">
            <div class="step complete">
                Basic Info
                <div class="node"></div>
            </div>
            <div class="step complete">
                More Info
                <div class="node"></div>
            </div>
            <div class="step in-progress">
                Licensing
                <div class="node"></div>
            </div>
            <div class="step">
                Signing
                <div class="node"></div>
            </div>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{{trans('form.fill_your_data')}}</p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form method="POST" action="{{ action('Auth\RegisterController@postDoctorStep3') }}" data-toggle="validator" role="form" id="registrationForm" enctype="multipart/form-data" class="custom-validation-form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-4 p-0">
                            <label>Registration State</label>
                            <select class="form-control" id="states" >
                                @foreach($states as $key => $state)
                                <option value="{{ $key  }}">{{ $state }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger  stae-error"></div>
                        </div>
                        <div class="col-xs-4 p-0">
                            <label>Your medical license</label>
                            <input type="text"  value="" class="form-control" id="medical_license" placeholder="Medical License">
                            <div class="license-error text-danger with-errors"></div>
                        </div>
                        <div class="col-xs-3 p-0">
                            <label>Valid Until</label>
                            <input type="text" name="valid_until" value="" class="form-control" id="valid_unti" placeholder="Valid Until" >
                            <div class="until-error text-danger with-errors"></div>
                        </div>
                        <div class="col-xs-1 m-t-23">
                            <button class="btn btn-danger  add-license"><span class="glyphicon glyphicon-plus"></span></button>
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <table class="table table-bordered">
                        <thead class="alert-info">
                        <th>Registration State</th>
                        <th>Medical license</th>
                        <th>Valid Until</th>
                        </thead>
                        <tbody class="bg-white license-body">
                            @if($user->doctorLicense)
                            @foreach($user->doctorLicense as $license)
                            <tr>

                                <td>{{$states[$license->registration_state]}}</td>
                                <td>{{$license->medical_license}}</td>
                                <td>{{$license->valid_until}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <input type="text" name="driver_license" value="{{session()->has('doctorStep3')? session()->get('doctorStep3')['driver_license']: ''}}" class="form-control" id="driver_license" placeholder="Driver License" required>
                    <input type="file" name="driver_license_file" class="m-t-5" id="driver_license_file">
                    <div class="help-block with-errors"></div>
                     @if($user->driver_license_image)
                    <img src='{{ asset ("/files/profiles/doctor-". $user->id ."/".$user->driver_license_image)}}' width="160" height="160">
                    @endif
                </div>
                <div class="form-group">
                    <label>Face Photo</label>
                    <input type="file" name="face_photo"  class="m-t-5" id="face_photo" >
                    <div class="help-block with-errors"></div>
                    @if($user->face_photo)
                    <img src='{{ asset ("/files/profiles/doctor-". $user->id ."/".$user->face_photo)}}' width="160" height="160">
                    @endif
                </div>
                <div class="row m-t-20">
                    <div class="col-xs-12 text-right">
                        <input type="hidden" name="user_id" value="{{session()->has('user_id')? session()->get('user_id'): ''}}">
                        <div class="col-xs-10">
                            <a href="{{ url('/auth/signup/step-2') }}" class="btn btn-primary btn-flat">Previous</a>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat">Next</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
