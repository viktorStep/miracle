@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')

@section('content')

<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="javascript:void(0)" >Profile info</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/edit') }}" >Edit profile</a></li>
            <li role="presentation"><a href="{{ URL::to('/profile/addresses') }}" >Addresses</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="profile-info">
                <h3>Profile Info</h3>
                <div class="x">
                    <div class="">
                        <div class="row profile-info-row m-t-30">
                            <div class="col-xs-12 col-md-3 profile-info-title">E-mail</div>
                            <div class="col-xs-12 col-md-9 profile-info-text">{{ auth()->user()->email }}</div>
                        </div>
                        <div class="row profile-info-row">
                            <div class="col-xs-12 col-md-3 profile-info-title">First Name</div>
                            <div class="col-xs-12 col-md-9 profile-info-text">{{ auth()->user()->first_name }}</div>
                        </div>
                        <div class="row profile-info-row">
                            <div class="col-xs-12 col-md-3 profile-info-title">Last Name</div>
                            <div class="col-xs-12 col-md-9 profile-info-text">{{ auth()->user()->last_name }}</div>
                        </div>
                        <div class="row profile-info-row">
                            <div class="col-xs-12 col-md-3 profile-info-title">Date Of Birth</div>
                            <div class="col-xs-12 col-md-9 profile-info-text">{{ date('m/d/Y', strtotime(auth()->user()->dob)) }}</div>
                        </div>
                        <div class="row profile-info-row">
                            <div class="col-xs-12 col-md-3 profile-info-title">Gender</div>
                            <div class="col-xs-12 col-md-9 profile-info-text">{{ auth()->user()->gender }}</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-3 profile-info-title">Has Recipe</div>
                            <div class="col-xs-12 col-md-9 profile-info-text">{{ auth()->user()->has_recipe }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection