@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')

@section('content')

<!-- Portfolio Grid Section -->
<section id="portfolio" class="blog-list">
    <div class="container">
            <div>
                <h2>{{ trans('post.' . $post->id . '.title') }}</h2>
                <div><small><a href="{{ URL::to('/blog/category/' . $post->category_id . '/' . strtolower(slugify(trans('blog_category.' . $post->category_id . '.title')))) }}">{{ trans('blog_category.' . $post->category_id . '.title') }}</a></small></div>
                <img src="{{ asset('/files/blog/'.date('m.Y').'/' . $post->file_name . '-m.' . $post->file_type) }}" />
                <div>{!! filterContent(trans('post.' . $post->id . '.content')) !!}</div>
            </div>
    </div>
</section>

@endsection
