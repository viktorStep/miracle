@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')

@section('content')

    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="blog-list">
        <div class="container">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-xs-12 col-md-offset-2 col-md-8">
                    <form method="POST" action="{{ action('Front\AccountController@postAddressesCreate') }}" data-toggle="validator" role="form" class="custom-validation-form">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <select class="form-control" id="states" name="state" required>
                                @foreach($states as $key => $state)
                                    <option value="{{ $key  }}" {{ (isset($address) && $address->state == $key) ? 'selected' : '' }}>{{ $state }}</option>
                                @endforeach
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="city" value="{{ isset($address) ? $address->city : '' }}" class="form-control" id="city" placeholder="City" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="address" value="{{ isset($address) ? $address->address : '' }}" class="form-control" id="address" placeholder="Address" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" name="zip_code" value="{{ isset($address) ? $address->zip_code : '' }}" class="form-control" id="zipcode" placeholder="Zip Code" required>
                            <div class="help-block with-errors"></div>
                        </div>
                        @if($addressTypes['billing'])
                            <div class="checkbox checkbox-inline m-0">
                                <label>
                                    <input type="checkbox" name="billing" {{ (isset($address) && $address->default_billing == 'yes') ? 'checked' : ''  }}> Billing
                                </label>
                            </div>
                        @endif
                        @if($addressTypes['shipping'])
                            <div class="checkbox checkbox-inline m-0">
                                <label>
                                    <input type="checkbox" name="shipping" {{ (isset($address) && $address->default_shipping == 'yes') ? 'checked' : ''  }}> Shipping
                                </label>
                            </div>
                        @endif
                        <input type="hidden" name="id" value="{{ isset($address) ? $address->id : '' }}">
                        <div class="row m-t-20">
                            <div class="col-xs-12 text-right">
                                <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                <a href="{{ URL::to('/profile/addresses') }}" class="btn btn-default btn-flat">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection