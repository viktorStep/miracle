@extends((Auth::check() && auth()->user()->role !='admin')  ? config('layout.'.auth()->user()->role): 'layouts.front.index')

@section('content')

    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="blog-list">
        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="{{ URL::to('/profile') }}">Profile info</a></li>
                <li role="presentation"><a href="{{ URL::to('/profile/edit') }}">Edit profile</a></li>
                <li role="presentation" class="active"><a href="javascript:void(0)">Addresses</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <h3>Profile Addresses</h3>
                    <div class="text-right m-t-10 m-b-10"><a class="btn btn-success" href="{{ URL::to('/profile/address/create') }}">Create</a></div>
                    <div class="row">
                        @if(count($addresses) > 0)
                            @foreach($addresses as $address)
                                <div class="col-xs-12 col-md-6 m-b-30">
                                    <div class="address">
                                        <div class="text-right m-b-10"><a href="{{ URL::to('/profile/address/edit/' . $address->id) }}">Edit</a></div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3 profile-info-title">Address</div>
                                            <div class="col-xs-12 col-md-9 profile-info-text">{{ $address->address }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3 profile-info-title">City</div>
                                            <div class="col-xs-12 col-md-9 profile-info-text">{{ $address->city }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3 profile-info-title">State</div>
                                            <div class="col-xs-12 col-md-9 profile-info-text">{{ $address->state }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-3 profile-info-title">Zip code</div>
                                            <div class="col-xs-12 col-md-9 profile-info-text">{{ $address->zip_code }}</div>
                                        </div>
                                        @if($address->default_billing == 'yes')
                                            <div class="address-type">Billing</div>
                                        @endif
                                        @if($address->default_shipping == 'yes')
                                            <div class="address-type">Shipping</div>
                                        @endif
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <h4>Data not found</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection