@extends('layouts.admin.index')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Posts" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="text-right m-b-30"><a href="{{ URL::to('/admin/post/create') }}" class="btn btn-primary">Add new Post</a></div>
                            <table class="table table-bordered table-hover datatable middle">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Locales</th>
                                    <th>Sort</th>
                                    <th>Status</th>
                                    <th data-orderable="false" style="width: 50px" class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($posts as $post)
                                    <tr>
                                        <td>{{ trans('post.' . $post->id . '.title') }}</td>
                                        <td>
                                            @foreach($post->titles as $title)
                                                <span class="label label-info m-r-10">{{ $title->locale }}</span>
                                            @endforeach
                                            <a href="/admin/post/translation/{{ $post->id }}" class="btn btn-primary translation-edit">Edit</a>
                                        </td>
                                        <td>{{ $post->sort }}</td>
                                        <td>{{ $post->status }}</td>
                                        <td>
                                            <a href="/admin/post/{{ $post->id }}" class="m-l-r-15"><i class="fa fa-edit"></i></a>
                                            <a href="javascript:void(0)" rel="{{ $post->id }}" data-type="post" class="recordDelete"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection