@extends('layouts.admin.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $page_title or "Patients" }}
            <small>{{ $page_description or null }}</small>
        </h1>
        <!-- You can dynamically generate breadcrumbs here -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" src="/files/avatar/{{$patient->image ? $patient->image : 'default-avatar.png' }}" alt="User profile picture">
                            <h3 class="profile-username text-center">{{ $patient->first_name . ' ' . $patient->last_name  }}</h3>
                            <p class="text-muted text-center">{{ $patient->role  }}</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#user-info" data-toggle="tab" aria-expanded="true">User Info</a></li>
                            <li class=""><a href="#pages-history" data-toggle="tab" aria-expanded="false">Pages history</a></li>
                            <li class=""><a href="#order-history" data-toggle="tab" aria-expanded="false">Order history</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="user-info">
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">E-mail</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->email }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Has recipe</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->has_recipe }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Gender</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->gender }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Date of birth</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->dob }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Phone</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->phone }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Address</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->address }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">State</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->state }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">City</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->city }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Zip code</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->zip_code }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Notes</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->notes }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Status</div>
                                    <div class="col-xs-12 col-md-5 user-info-value">{{ $patient->status == 'rejected' ? $patient->status . ' ( ' . $patient->status_reason . ' ) ' : $patient->status }}</div>
                                    <div class="col-xs-12 col-md-4 user-info-value">
                                        <select id="statuses" class="form-control">
                                            <option value="0">-- Change status --</option>
                                            <option value="approved">Aprove</option>
                                            <option value="blocked">Block</option>
                                            <option value="rejected">Reject</option>
                                        </select>
                                        <textarea id="status_reason" class="form-control hidden"></textarea>
                                        <input type="button" class="btn btn-default" id="changeStatus" data-id="{{ $patient->id }}" value="Save" />
                                    </div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Ip</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->ip }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">User agent</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->user_agent }}</div>
                                </div>
                                <div class="row user-info-item">
                                    <div class="col-xs-12 col-md-3 user-info-title">Registration date</div>
                                    <div class="col-xs-12 col-md-9 user-info-value">{{ $patient->created_at }}</div>
                                </div>
                            </div>
                            <div class="tab-pane" id="pages-history">
                                pages history content
                            </div>
                            <div class="tab-pane" id="order-history">
                                order history content
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection