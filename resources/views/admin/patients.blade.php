@extends('layouts.admin.index')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{ $page_title or "Patients" }}
                <small>{{ $page_description or null }}</small>
            </h1>
            <!-- You can dynamically generate breadcrumbs here -->
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-hover datatable middle" data-order="[[ 1, &quot;asc&quot; ]]">
                                <thead>
                                <tr>
                                    <th data-orderable="false" style="width: 26px">Avatar</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>E-mail</th>
                                    <th>Gender</th>
                                    <th>Date of birth</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($patients as $patient)
                                    <tr>
                                        <td><img src="/files/avatar/{{$patient->image ? $patient->image : 'default-avatar.png' }}" class="img-thumbnail" style="width: 50px"></td>
                                        <td><a href="{{ URL::to('/admin/patient/' . $patient->id) }}">{{ $patient->first_name }}</a></td>
                                        <td>{{ $patient->last_name }}</td>
                                        <td>{{ $patient->email }}</td>
                                        <td>{{ $patient->gender }}</td>
                                        <td>{{ $patient->dob }}</td>
                                        <td>{{ $patient->status }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection