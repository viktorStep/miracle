<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/login', 'Auth\LoginController@logout');
// Admin Authentication routes...
Route::get('admin/auth/login', 'Admin\AuthController@getLogin');
Route::post('admin/auth/login', 'Admin\AuthController@postLogin');
Route::get('admin/auth/logout', 'Admin\AuthController@getLogout');

Route::group(['prefix' => 'admin', 'middleware' => ['auth.admin']], function () {
    Route::get('/', 'Admin\DashboardController@index');
    Route::get('/patients', 'Admin\PatientsController@index');
    Route::get('/patient/{id}', 'Admin\PatientsController@singleView');
    Route::get('/doctors', 'Admin\DoctorsController@index');
    Route::get('/doctor/{id}', 'Admin\DoctorsController@singleView');
    Route::any('/user/status', 'Admin\UsersController@changeStatus');

    //Blog Category
    Route::get('/categories', 'Admin\CategoriesController@index');
    Route::get('/category/create', 'Admin\CategoriesController@getCreate');
    Route::post('/category/create', 'Admin\CategoriesController@postCreate');
    Route::get('/category/translation/{category_id}', 'Admin\CategoriesController@getTranslation');
    Route::post('/category/translation/{category_id}', 'Admin\CategoriesController@postTranslation');
    Route::get('/category/{id}', 'Admin\CategoriesController@getUpdate');
    Route::post('/category/{id}', 'Admin\CategoriesController@postUpdate');
    Route::get('/category/delete/{id}', 'Admin\CategoriesController@delete');
    //Product Category
    Route::get('/product-categories', 'Admin\ProductCategoriesController@index');
    Route::get('/product-category/create', 'Admin\ProductCategoriesController@getCreate');
    Route::post('/product-category/create', 'Admin\ProductCategoriesController@postCreate');
    Route::get('/product-category/translation/{category_id}', 'Admin\ProductCategoriesController@getTranslation');
    Route::post('/product-category/translation/{category_id}', 'Admin\ProductCategoriesController@postTranslation');
    Route::get('/product-category/{id}', 'Admin\ProductCategoriesController@getUpdate');
    Route::post('/product-category/{id}', 'Admin\ProductCategoriesController@postUpdate');
    Route::get('/product-category/delete/{id}', 'Admin\ProductCategoriesController@delete');
    //Posts
    Route::get('/posts', 'Admin\PostsController@index');
    Route::get('/post/create', 'Admin\PostsController@getCreate');
    Route::post('/post/create', 'Admin\PostsController@postCreate');
    Route::post('/post/upload', 'Admin\PostsController@postUpload');
    Route::post('/post/featured', 'Admin\PostsController@postChangeFeatruedImage');
    Route::get('/post/translation/{post_id}', 'Admin\PostsController@getTranslation');
    Route::post('/post/translation/{post_id}', 'Admin\PostsController@postTranslation');
    Route::post('/post/{id}', 'Admin\PostsController@postUpdate');
    Route::get('/post/{id}', 'Admin\PostsController@getUpdate');
    Route::get('/post/delete/{id}', 'Admin\PostsController@delete');
    //languages
    Route::get('/languages', 'Admin\LanguagesController@index');
    Route::get('/language/create', 'Admin\LanguagesController@getCreate');
    Route::post('/language/create', 'Admin\LanguagesController@postCreate');
    Route::get('/language/{id}', 'Admin\LanguagesController@getUpdate');
    Route::post('/language/{id}', 'Admin\LanguagesController@postUpdate');
    Route::get('/language/delete/{id}', 'Admin\LanguagesController@delete');
});

Route::group(['prefix' => \UriLocalizer::localeFromRequest(), 'middleware' => 'localize'], function () {

//Doctor and Patient Authentication routes...
    Route::get('/', ['as' => 'home', 'uses' => 'IndexController@index']);
    Route::get('/blog', ['as' => 'blog', 'uses' => 'PostController@index']);
    Route::get('/blog/category/{id}/{slug}', ['as' => 'blog', 'uses' => 'PostController@getByCategory']);
    Route::get('/blog/{id}/{slug}', ['as' => 'blog', 'uses' => 'PostController@single']);

    Route::get('/auth/signin', 'Auth\LoginController@showLoginForm');
    Route::post('/auth/signin', 'Auth\LoginController@postLogin');
    Route::get('/auth/signup', 'Auth\RegisterController@showRegistrationForm');
    Route::post('/auth/signup', 'Auth\RegisterController@postRegister');

    Route::group(['domain' => 'miraclecrop.dev'], function () {
        //step 1
        Route::get('/auth/signup/step-1', 'Auth\RegisterController@getPatientStep1');
        Route::post('/auth/signup/step-1', 'Auth\RegisterController@postPatientStep1');
        //step 2
        Route::get('/auth/signup/step-2', 'Auth\RegisterController@getPatientStep2');
        Route::post('/auth/signup/step-2', 'Auth\RegisterController@postPatientStep2');
        //step 3 
        Route::get('/auth/signup/step-3', 'Auth\RegisterController@getPatientStep3');
        Route::post('/auth/signup/step-3', 'Auth\RegisterController@postPatientStep3');
        //step 4
        Route::get('/auth/signup/step-4', 'Auth\RegisterController@getPatientStep4');
        Route::post('/auth/signup/step-4', 'Auth\RegisterController@postPatientStep4');
        //step 5
        Route::get('/auth/signup/step-5', 'Auth\RegisterController@getPatientStep5');
        Route::post('/auth/signup/step-5', 'Auth\RegisterController@postPatientStep5');
    });
    
     Route::group(['domain' => 'doctor.miraclecrop.dev'], function () {
        //step 1
        Route::get('/auth/signup/step-1', 'Auth\RegisterController@getDoctorStep1');
        Route::post('/auth/signup/step-1', 'Auth\RegisterController@postDoctorStep1');
        //step 2
        Route::get('/auth/signup/step-2', 'Auth\RegisterController@getDoctorStep2');
        Route::post('/auth/signup/step-2', 'Auth\RegisterController@postDoctorStep2');
        //step 3
        Route::get('/auth/signup/step-3', 'Auth\RegisterController@getDoctorStep3');
        Route::post('/auth/signup/step-3', 'Auth\RegisterController@postDoctorStep3');
        Route::post('/doctor/license', 'Auth\RegisterController@postDoctorLicense');
        //step 4
        Route::get('/auth/signup/step-4', 'Auth\RegisterController@getDoctorStep4');
        Route::post('/auth/signup/step-4', 'Auth\RegisterController@postDoctorStep4');
    });

    Route::get('/auth/logout', 'Auth\LoginController@logout');

    // Password reset link request routes...
    Route::get('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email.post', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);

// Password reset routes...
    Route::get('password/reset/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

    Route::get('/registration-success', ['as' => 'registration-success', 'uses' => 'Auth\RegisterController@showRegistrationInfo']);
    Route::get('/confirmation-failed', ['as' => 'confirmation-failed', 'uses' => 'Auth\RegisterController@showRegistrationInfo']);
    Route::get('/confirmation-success', ['as' => 'confirmation-success', 'uses' => 'Auth\RegisterController@showRegistrationInfo']);
    Route::get('/confirm-email/{confirmationToken}', ['as' => 'confirm-email', 'uses' => 'Auth\RegisterController@confirmEmail'])->name('confirmationToken');

    Route::get('/profile', 'Front\AccountController@getProfile');
    Route::get('/profile/edit', 'Front\AccountController@getEditProfile');
    Route::post('/profile/edit', 'Front\AccountController@postEditProfile');
    Route::get('/profile/addresses', 'Front\AccountController@getAddresses');
    Route::get('/profile/address/create', 'Front\AccountController@getAddressesEdit');
    Route::post('/profile/address/create', 'Front\AccountController@postAddressesCreate');
    Route::get('/profile/address/edit/{id}', 'Front\AccountController@getAddressesEdit');
    Route::post('/profile/address/edit', 'Front\AccountController@postAddressesCreate');

//Doctors routes...
    Route::get('/doctor', ['as' => 'doctor.account', 'uses' => 'Front\Doctor\IndexController@index'])->middleware('auth.doctor');


//Patient routes...
    Route::get('/patient', ['as' => 'patient.account', 'uses' => 'Front\Patient\IndexController@index'])->middleware('auth.patient');

      Route::post('/signature', 'Auth\RegisterController@postSignature');
});

