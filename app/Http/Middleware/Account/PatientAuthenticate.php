<?php

namespace App\Http\Middleware\Account;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class PatientAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
      
        $role = $this->auth->user()->role;
 
        if ($role != config('roles.patient')) {
            return redirect()->route($role.'.account')->with('status', 'You are not authorized.');
        } 

        return $next($request);
    }
}
