<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller {

    public function index() {
        if (Auth::check() && auth()->user()->role !='admin') {
            $view = 'front.' . auth()->user()->role . '.index';
        } else{
            $view = 'front.index';
        }
        return view($view);
    }

}
