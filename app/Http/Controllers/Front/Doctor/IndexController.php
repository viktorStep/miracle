<?php

namespace App\Http\Controllers\Front\Doctor;

use App\Http\Controllers\Controller;

class IndexController extends Controller {
	
	  public function __construct() {
        $this->middleware('auth.doctor');
        
    }

    public function index() {
        return view('front.doctor.index');
    }

}


