<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\User;
use App\Models\Address;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller {

    public function __construct() {
        $this->middleware('role');
    }

    public function changeStatus(Request $request) {
        if ($request->id) {
            $user = User::find($request->id);
            $user->status = $request->status;

            if (isset($request->status_reason))
                $user->status_reason = $request->status_reason;

            $user->save();
            return response()->json([
                        'status' => $request->status
            ]);
        }
    }
    
    public function getProfile(User $user) {
        
        return view('front.profile_page');
    }

    public function getEditProfile(User $user) {

        return view('front.profile_page_edit');
    }

    public function getAddresses(User $user) {

        $addresses = Address::where('user_id', Auth::id())->get();

        return view('front.profile_addresses', ['addresses' => $addresses]);
    }

    public function getAddressesEdit($id = null, User $user) {

        $addressTypes = array('billing' => true, 'shipping' => true);
        $addresses = Address::where('user_id', Auth::id())->get();

        if(count($addresses) > 0) {
            foreach ($addresses as $address) {
                if ($address->default_billing == 'yes')
                    $addressTypes['billing'] = ($id && $id == $address->id) ? true : false;
                if ($address->default_shipping == 'yes')
                    $addressTypes['shipping'] = ($id && $id == $address->id) ? true : false;
            }
        }

        $data = array('states' => User::$states, 'addressTypes' => $addressTypes);

        if($id) {
            $address = Address::find($id);
            if(count($address) > 0)
                $data['address'] = $address;
        }

        return view('front.profile_address_create', $data);

    }
    
    public function postEditProfile(Request $request) {
        $user = User::find(auth()->user()->id);
        $user->first_name = $request->firstname;
        $user->last_name = $request->lastname;
        $user->dob = $request->dob;
        $user->gender = $request->gender;
        $user->has_recipe = isset($request->has_recipe) ? 'yes' : 'no';
        $user->save();
        
        return redirect()->back()->with('success', 'Your data updated successfully');
    }

    public function postAddressesCreate(Request $request) {
        if($request->id)
           $address = Address::find($request->id);
        else
            $address = new Address();

        $validator = Validator::make($request->all(), $address::$rules);

        if ($validator->fails()) {
            return redirect('profile/address/' . ($request->id ? 'edit/' . $request->id : 'create'))
                ->withErrors($validator)
                ->withInput();
        }

        $address->user_id = Auth::id();
        $address->state = $request->state;
        $address->city = $request->city;
        $address->address = $request->address;
        $address->zip_code = $request->zip_code;
        $address->default_billing = $request->billing ? 'yes' : 'no';
        $address->default_shipping = $request->shipping ? 'yes' : 'no';
        $address->save();

        return redirect('/profile/addresses');
    }

}
