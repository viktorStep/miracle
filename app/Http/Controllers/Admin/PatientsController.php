<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;

class PatientsController extends UsersController {

    public function index() {
        $patients = User::all()->where('role', 'patient');

        return view('admin.patients', ['patients' => $patients]);
    }

    public function singleView($id) {
        $patient = User::find($id);

        return view('admin.patientsSingle', ['patient' => $patient]);
    }

}
