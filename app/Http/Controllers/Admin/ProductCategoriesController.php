<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Waavi\Translation\Repositories\TranslationRepository;
use Illuminate\Http\Request;
use Validator;
use App\Models\Category;
use App\Models\Language;
use App\Models\Translations;

class ProductCategoriesController extends Controller {

    protected $translation;

    public function __construct() {
        $this->middleware('auth.admin');
        $this->translation = new Translations();
    }

    public function index() {
        $categories = Category::where('type', 'product')->get();

        foreach ($categories as $key => $category) {
            $categories[$key]['translations'] = $this->translation->getTranslationsByGroupAndItem('product_category', $category->id . '.title');
        }

        return view('admin.productCategories', ['categories' => $categories]);
    }

    public function getCreate() {

        return view('admin.productCategoryCreate');
    }

    public function postCreate(Request $request, TranslationRepository $TranslationRepository) {
        $category = new Category();

        $validator = Validator::make(['title' => $request->title], Category::$rules);

        if ($validator->fails()) {
            return redirect('admin/product-category/create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $category->sort = $request->sort;
        $category->status = $request->status;
        $category->type = 'product';

        $category->save();

        $TranslationRepository->create([
            'locale' => 'en',
            'namespace' => '*',
            'group' => 'product_category',
            'item' => $category->id . '.title',
            'text' => $request->title,
        ]);

        return redirect('/admin/product-categories');
    }

    public function getUpdate($id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $category = Category::find($id);
            $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');
            $categoryTranslation = json_decode($TranslationRepository->search('en', 'product_category.' . $category->id . '.title'));

            return view('admin.productCategoryCreate', ['category' => $category, 'languages' => $languages, 'title' => $categoryTranslation[0]->text]);
        }
    }

    public function postUpdate(Request $request, $id, TranslationRepository $TranslationRepository) {
        if (isset($id) && !empty($id)) {
            $category = Category::find($id);
            $validator = Validator::make(['title' => $request->title], Category::$rules);

            if ($validator->fails()) {
                return redirect('admin/product-category/' . $id)
                                ->withErrors($validator)
                                ->withInput();
            }

            $category->sort = $request->sort;
            $category->status = $request->status;
            $categoryTranslation = json_decode($TranslationRepository->search('en', 'product_category.' . $category->id . '.title'));
            
            $translations = $this->translation->getTranslationsByGroupAndItem('product_category', $category->id . '.title');
            foreach($translations as $translation){
                if($translation->locale !='en'){
                      $this->translation->deleteSingleTranslationsByGroupAndItem($translation->locale, 'product_category', $category->id . '.title');
                }
            }
            $category->save();

            $TranslationRepository->update($categoryTranslation[0]->id, $request->title);

            return redirect('/admin/product-categories');
        }
    }

    public function getTranslation($category_id) {
        $category = Category::find($category_id);
        $translations = $this->translation->getTranslationsByGroupAndItem('product_category', $category->id . '.title');

        $languages = Language::where('deleted_at', NULL)->pluck('name', 'locale');

        return view('admin.productCategoryTranslation', ['category' => $category, 'languages' => $languages, 'translations' => $translations]);
    }

    public function postTranslation($category_id, Request $request, TranslationRepository $TranslationRepository) {

        foreach ($request->title as $language_type => $language_text) {
            $single_translation = $this->translation->getSingleTranslationsByGroupAndItem($language_type, 'product_category', $category_id . '.title');
            if ($language_type != 'en') {
                if (!empty($language_text[0])) {
                    if (isset($single_translation) && $single_translation->locale == $language_type) {
                        $TranslationRepository->update($single_translation->id, $language_text[0]);
                    } else {
                        $TranslationRepository->create([
                            'locale' => $language_type,
                            'namespace' => '*',
                            'group' => 'product_category',
                            'item' => $category_id . '.title',
                            'text' => $language_text[0],
                        ]);
                    }
                } else if (empty($language_text[0]) && isset($single_translation)) {
                    $this->translation->deleteSingleTranslationsByGroupAndItem($language_type, 'product_category', $category_id . '.title');
                }
            }
        }

        return redirect('/admin/product-categories');
    }

    public function delete($id) {
        if (isset($id) && !empty($id)) {
            $category = Category::find($id);

            $this->translation->deleteTranslationsByGroupAndItem('product_category', $id . '.title');

            $category->delete();

            return redirect('/admin/product-categories');
        }
    }

}
