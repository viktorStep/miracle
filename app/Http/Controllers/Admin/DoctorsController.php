<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;

class DoctorsController extends UsersController {

    public function index() {
        $doctors = User::all()->where('role', 'doctor');

        return view('admin.doctors', ['doctors' => $doctors]);
    }

    public function singleView($id) {
        $doctor = User::find($id);

        return view('admin.doctorsSingle', ['doctor' => $doctor]);
    }

}
