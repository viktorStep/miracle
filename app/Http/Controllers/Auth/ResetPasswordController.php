<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset requests
      | and uses a simple trait to include this behavior. You're free to
      | explore this trait and override any methods you wish to tweak.
      |
     */

use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null) {

        return view('front.auth.reset')->with(
                        ['token' => $token, 'email' => $request->email]
        );
    }

    protected function resetPassword($user, $password) {
        $user->password = bcrypt($password);

        $user->confirm_token = null;


        $user->save();
    }

    protected function sendResetResponse($response) {
        return redirect()->action('Auth\LoginController@showLoginForm')
                        ->with('status', "Passsword reset");
    }

}
