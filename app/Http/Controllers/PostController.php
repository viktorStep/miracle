<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller {

    public function index() {

        $posts = Post::where('status', 'published')->where('is_featured', 'yes')
                        ->join('attachments', 'attachments.post_id', '=', 'posts.id')
                        ->select('posts.*', 'attachments.file_name', 'attachments.file_type')->get();

        return view('front.blog', ['posts' => $posts]);
    }

    public function single(Request $request, $id, $slug) {
        $post = Post::where('posts.id', $id)->where('status', 'published')->where('is_featured', 'yes')
                        ->join('attachments', 'attachments.post_id', '=', 'posts.id')
                        ->select('posts.*', 'attachments.file_name', 'attachments.file_type')->first();

        return view('front.blog_single', ['post' => $post]);
    }
    
    public function getByCategory(Request $request, $id, $slug) {
        $posts = Post::where('posts.category_id', $id)->where('status', 'published')->where('is_featured', 'yes')
                        ->join('attachments', 'attachments.post_id', '=', 'posts.id')
                        ->select('posts.*', 'attachments.file_name', 'attachments.file_type')->get();

        return view('front.blog_by_category', ['posts' => $posts, 'category_id' => $id]);
    }

}
