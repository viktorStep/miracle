<?php

namespace App\Validators;

use Illuminate\Validation\Validator;

class CustomValidator extends Validator {

    public function validateOver21($attribute, $value, $parameters)
    {
        $now = time(); // or your date as well
        $dob = strtotime($value);
        $datediff = $now - $dob;

        $age = floor($datediff / (60 * 60 * 24 * 364));
        if($age < 21)
            return false;
        return true;
    }

}