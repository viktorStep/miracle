<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Attachment extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'attachments';
    protected $fillable = [
        'sort',
        'status'
    ];
    public static $rules = [
        'title' => 'required',
        'category' => 'required',
        'content' => 'required',
        'excerpt' => 'required',
    ];

    public function post() {
        return $this->belongsTo('App\Models\Post', 'post_id');
    }

    public function getImage() {
        $image = '/img/default-image.jpg';
        if (!empty($this->file_name)) {
            $image = $this->file_name;
        }
        return $image;
    }

//    public function getSmallImage() {
//        $image = '/img/default-image.jpg';
//        if (!empty($this->small_image)) {
//            $image = $this->small_image;
//        }
//        return $image;
//    }
}
