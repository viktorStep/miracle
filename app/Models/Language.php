<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Language extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'translator_languages';

    protected $fillable = [
        'locale',
        'name'
    ];

    public static $rules = [
        'name' => 'required',
        'locale' => 'required'
    ];

}