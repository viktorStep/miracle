<?php

use App\Models\Translations;
use Sunra\PhpSimple\HtmlDomParser;

if (!function_exists('translate')) {

    function translate($url) {
        $url_array = explode('/', $url);

        if (count($url_array) == 5) {
            $translation_text = app(Translations::class)->getSingleTranslationsByGroupAndItem($url_array[1], 'post', $url_array[3] . '.title');
            if ($translation_text) {
                return '/' . $url_array[1] . '/' . $url_array[2] . '/' . $url_array[3] . '/' . slugify($translation_text->text);
            } else {
                return $url;
            }
        } else {
            return $url;
        }
    }

}

if (!function_exists('slugify')) {

    function slugify($text) {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

}

if (!function_exists('filterContent')) {

    function filterContent($content) {
        $dom = HtmlDomParser::str_get_html($content);

        if ($dom->find('.youtube-video')) {
            foreach ($dom->find('.youtube-video') as $key => $element) {
                $video = $dom->find('.youtube-video', $key);
                if ($video->rel) {
                    $video->outertext = '<figure class="iframe-cont"><iframe width="800" height="450" src="http://www.youtube.com/embed/'. $video->rel .'?wmode=opaque&autoplay=0&showinfo=0&autohide=1&rel=0" frameborder="0" allowfullscreen></iframe></figure>';
                }
            }
            $content = $dom->save();
        }
        return $content;
    }

}

